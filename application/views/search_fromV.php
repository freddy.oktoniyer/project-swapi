<div class="page-title">
    <h3>Data People</h3>
    <p class="text-subtitle text-muted">Search People</p>
</div>
<div class="card">
    <div class="card-header">
        <div class="col-12">
            <div class="row">
                <div class=" col-6 text-left">Data People</div>
            </div>
        </div>
    </div>
    <div class="card-body ">

        <form id="searchForm">
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <input type="text" id="name" name="name" class="form-control" placeholder="Search by Person Name" required>
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
                <div id="results"></div>
            </div>

    </div>
    </form>
</div>
<!-- Planet Info Modal -->
<div class="modal fade" id="planetInfoModal" tabindex="-1" aria-labelledby="planetInfoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="planetInfoModalLabel">Planet Information</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function showLoading() {
        // Show loading spinner when button is clicked
        document.getElementById('loading').style.display = 'block';
    }
    $(document).ready(function() {
        $('#searchForm').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('swapi/searchPerson'); ?>",
                data: $(this).serialize(),
                success: function(data) {
                    $('#results').html(''); // Clear previous results
        if (data.length === 0) {
            // Jika tidak ada hasil, tampilkan pesan "No result"
            $('#results').html('<div class="alert alert-danger" role="alert">No result found.</div>');
        } else {
                    var html = '<table class="table table-borderless"><tbody>';
                    data.forEach(function(person) {
                        html += `<tr>
                                    <td>Person Name :</td>
                                    <td>${person.name}</td>
                                 </tr>
                                 <tr>
                                    <td>Person Gender :</td>
                                    <td>${person.gender}</td>
                                 </tr>
                                 <tr>
                                    <td>Person Homeworld :</td>
                                    <td><a href="#" onclick="getPlanet('${person.homeworld}')">${person.homeworld_name}</a></td>
                                 </tr>
                                 <tr>
                                 <td>Starship</td>
                                   
                                    <td>${person.starships.map(s => `${s.name} (${s.model})`).join(', ') || '-'}</td>
                                 <tr>
                                 `;
                    });
                    html += '</tbody></table>';
                    $('#results').html(html);
                }
            }
            });
        });
    });
    function getPlanet(url) {
        $.ajax({
            url: url,
            success: function(data) {
                var modalContent = `
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <td>Planet Name :</td>
                            <td>${data.name}</td>
                        </tr>
                        <tr>
                            <td>Planet Population :</td>
                            <td>${data.population}</td>
                        </tr>
                    </tbody>
                </table>
            `;
                $('#planetInfoModal .modal-body').html(modalContent);
                $('#planetInfoModal').modal('show');
            }
        });
    }
</script>