<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Swapi</title>

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">    

    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/chartjs/Chart.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/select2/css/select2.css">

    <!-- jQuery library -->
    <!-- <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script> -->
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Datatable CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/datatables/media/css/jquery.dataTables.css">

    <script src="<?php echo base_url()?>assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/datatables/media/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/datatables/media/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/datatables/media/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/datatables/media/js/jszip.min.js"></script>    

    <!-- Select2 library -->
    <!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/select2/css/select2.min.css">
    <script src="<?php echo base_url()?>assets/select2/js/select2.min.js"></script> -->

    <!-- Include Choices CSS -->
    <!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/choices.js/choices.min.css" /> -->

    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/app.css">
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/swapi_logo.png" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css">
    
    <link href="<?php echo base_url('assets/bootstrap-toastr/toastr.min.css')?>" rel="stylesheet" type="text/css" />
</head>


<style>

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

.tab {
  display: none;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #04AA6D;
}
</style>

<body>
    <div id="app">
        <div id="sidebar" class='active'>
            <div class="sidebar-wrapper active">
                <!-- <div class="sidebar-header">
                    <img src="<?php echo base_url()?>assets/images/swapi_logo.png" alt="" srcset="" style="width: 80%; height: 50%;">
                </div> -->
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class='sidebar-title'>Pilihan Menu</li>
                            <li class='sidebar-item <?php if($this->uri->segment(2)=="employee"){echo "active";}?> '>
                                <a href="<?php echo base_url()?>" class='sidebar-link'>
                                    <i class="fa fa-user" aria-hidden="true" style="color: #5a8dee;"></i>
                                    <span>People</span>
                                </a>
                            </li>
                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>
        <div id="main">
            <nav class="navbar navbar-header navbar-expand navbar-light">
                <a class="sidebar-toggler" href="#"><span class="navbar-toggler-icon"></span></a>
                <button class="btn navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav d-flex align-items-center navbar-light ms-auto">
                        <li class="dropdown">
                            <a href="#" data-bs-toggle="dropdown"
                                class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                                <div class="avatar me-1">
                                    <img src="<?php echo base_url()?>assets/images/avatar/avatar-s-1.png" alt="" srcset="">
                                </div>
                                <div class="d-none d-md-block d-lg-inline-block"><p></p></div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="#"><i data-feather="log-out"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="main-content container-fluid">
                <section class="section">
                    <?php echo $isi; ?>
                </section>
            </div>

            <footer>
                <div class="footer clearfix mb-0 text-muted">
                    <div class="float-start">
                        <p><?php echo date('Y');?> &copy; Freddy Oktoniyer S</a> All rights reserved.</p>
                    </div>
                    <div class="float-end">
                        <p>Version 1.0.0</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="<?php echo base_url()?>assets/js/feather-icons/feather.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/app.js"></script>

    <script src="<?php echo base_url()?>assets/vendors/select2/js/select2.min.js"></script>

    <script src="<?php echo base_url()?>assets/vendors/chartjs/Chart.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/apexcharts/apexcharts.min.js"></script>
    <!-- <script src="<?php echo base_url()?>assets/js/pages/dashboard.js"></script> -->

    <!-- Datatable JavaScript -->
    <!-- <script src="<?php echo base_url()?>assets/vendors/simple-datatables/simple-datatables.js"></script>
    <script src="<?php echo base_url()?>assets/js/vendors.js"></script> -->

    <!-- Include Choices JavaScript -->
    <script src="<?php echo base_url()?>assets/vendors/choices.js/choices.min.js"></script>

    <script src="<?php echo base_url()?>assets/js/main.js"></script>

    <!-- END THEME LAYOUT SCRIPTS -->
    <script src="<?php echo base_url('assets/bootstrap-toastr/toastr.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/jquery.pulsate.min.js')?>" type="text/javascript"></script>
</body>

<script type="text/javascript">
        
 


</script> 

</html>