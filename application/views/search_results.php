<!DOCTYPE html>
<html>
<head>
    <title>Search Results</title>
</head>
<body>
    <?php if (!empty($results)): ?>
        <ul>
            <?php foreach ($results as $person): ?>
                <li>
                    Name: <?php echo $person['name']; ?><br>
                    Gender: <?php echo $person['gender']; ?><br>
                    Homeworld: <a href="<?php echo site_url('swapi/planet/' . basename($person['homeworld'])); ?>">View Homeworld</a><br>
                    Starships: 
                    <?php if (!empty($person['starships'])): ?>
                        <ul>
                            <?php foreach ($person['starships'] as $shipUrl): ?>
                                <li><?php echo basename($shipUrl); // Placeholder for starship name ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php else: ?>
                        -
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <p>No results found.</p>
    <?php endif; ?>
</body>
</html>
