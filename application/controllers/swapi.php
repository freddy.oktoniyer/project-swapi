<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class swapi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('file');
     
        
        $this->load->driver('cache', array('adapter' => 'file'));
    }

 
    function index()
	{    
		$this->data['data'] = "";
        // $this->data['combined_data'] = $this->fetchAllPeopleFromSWAPI();
		$this->data['isi'] = $this->load->view('search_fromV', $this->data, TRUE);
		$this->load->view('layout', $this->data);
	}

   
    private function callSwapi($endpoint) {
        $cacheKey = md5($endpoint);
        $cachedData = $this->cache->get($cacheKey);

        if ($cachedData) {
            return $cachedData;
        } else {
            $response = file_get_contents($endpoint);
            $data = json_decode($response, true);
            // Cache for 1 day
            $this->cache->save($cacheKey, $data, 86400);
            return $data;
        }
    }

    public function searchPerson() {
        $name = $this->input->post('name');
        $data = $this->callSwapi("https://swapi.dev/api/people/?search=" . urlencode($name));

        $result = [];
        if (!empty($data['results'])) {
            foreach ($data['results'] as $person) {
                $homeworldResponse = $this->callSwapi($person['homeworld']);
                $starships = [];
                foreach ($person['starships'] as $starshipUrl) {
                    $starshipData = $this->callSwapi($starshipUrl);
                    $starships[] = [
                        'name' => $starshipData['name'],
                        'model' => $starshipData['model']
                    ];
                }
                $result[] = [
                    'name' => $person['name'],
                    'gender' => $person['gender'],
                    'homeworld' => $person['homeworld'],
                    'homeworld_name' => $homeworldResponse['name'],
                    'starships' => $starships
                ];
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function planet($id) {
        $data = $this->callSwapi("https://swapi.dev/api/planets/{$id}/");
        $result = [
            'name' => $data['name'],
            'population' => $data['population']
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }
}
